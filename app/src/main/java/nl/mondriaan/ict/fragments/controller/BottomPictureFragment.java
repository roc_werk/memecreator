package nl.mondriaan.ict.fragments.controller;

import android.content.ClipData;
import android.content.ClipDescription;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.DragEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import nl.mondriaan.ict.fragments.R;
import nl.mondriaan.ict.fragments.model.MemeImage;
import nl.mondriaan.ict.fragments.model.MemeModel;
import nl.mondriaan.ict.fragments.util.Logger;

public class BottomPictureFragment extends Fragment implements MemeModel.MemeListener {

    private TextView topMemeText;
    private TextView bottomMemeText;
    private ImageView imageView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.bottom_picture_fragment, container, false);

        RelativeLayout mainLayout = (RelativeLayout) view.findViewById(R.id.memeLayout);
        topMemeText = (TextView) view.findViewById(R.id.topMemeText);
        bottomMemeText = (TextView) view.findViewById(R.id.bottomMemeText);
        imageView = (ImageView) view.findViewById(R.id.imageView);

        // Creating a touchListener we can use for both the top and bottom text
        View.OnTouchListener touchListener = new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    ClipData data = ClipData.newPlainText("", "");
                    View.DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(v);
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                        v.startDragAndDrop(data, shadowBuilder, v, 0);
                    } else {
                        v.startDrag(data, shadowBuilder, v, 0);
                    }
                    return true;
                } else {
                    return false;
                }
            }
        };
        topMemeText.setOnTouchListener(touchListener);
        bottomMemeText.setOnTouchListener(touchListener);

        mainLayout.setOnDragListener(new MyDragListener());

        return view;
    }

    @Override
    public void update(String top, String bottom) {
        topMemeText.setText(top);
        bottomMemeText.setText(bottom);
    }

    @Override
    public void updateImage(MemeImage memeImage) {
        imageView.setImageBitmap(memeImage.getImage());
    }

    @Override
    public void updateTextSize(int top, int bottom) {
        topMemeText.setTextSize(top);
        bottomMemeText.setTextSize(bottom);
    }

    /**
     * OnDragListener
     */
    private class MyDragListener implements View.OnDragListener {

        private Point startPoint = new Point();
        private boolean isDropped = false;
        private View currentView;
        private RelativeLayout.LayoutParams layoutParams;

        @Override
        public boolean onDrag(View v, DragEvent event) {
            // Defines a variable to store the action type for the incoming event
            final int action = event.getAction();
            // Handles each of the expected events
            switch(action) {

                case DragEvent.ACTION_DRAG_STARTED:

                    // Determines if this View can accept the dragged data
                    if (event.getClipDescription().hasMimeType(ClipDescription.MIMETYPE_TEXT_PLAIN)) {

                        currentView = (View) event.getLocalState();
                        if(currentView == null) return false;

                        // Setting the default values on Start
                        startPoint.set((int) currentView.getX(), (int) currentView.getY());
                        isDropped = false;

                        layoutParams = (RelativeLayout.LayoutParams) currentView.getLayoutParams();
                        layoutParams.topMargin = 0;
                        // Removing the layout below because we are going to set the margin based on
                        // the Y-offset from the top
                        layoutParams.addRule(RelativeLayout.BELOW, 0);
                        currentView.setVisibility(View.INVISIBLE);
                        currentView.setLayoutParams(layoutParams);

                        // Invalidate the view to force a redraw in the new tint
                        currentView.invalidate();
                        v.invalidate();

                        // returns true to indicate that the View can accept the dragged data.
                        return true;

                    }

                    // Returns false. During the current drag and drop operation, this View will
                    // not receive events again until ACTION_DRAG_ENDED is sent.
                    return false;

                // All these events we won't use
                case DragEvent.ACTION_DRAG_ENTERED:
                case DragEvent.ACTION_DRAG_LOCATION:
                case DragEvent.ACTION_DRAG_EXITED:
                    return true;

                case DragEvent.ACTION_DROP:
                    currentView = (View) event.getLocalState();
                    if(currentView == null) return false;
                    isDropped = true;

                    // Setting the new position to the new Y position
                    layoutParams.topMargin = (int) event.getY();
                    currentView.setLayoutParams(layoutParams);

                    currentView.invalidate();
                    v.invalidate();

                    return true;

                case DragEvent.ACTION_DRAG_ENDED:
                    // If the drag is ended and the drop wasn't triggered we need to set back the
                    // currentView to it's prev position and make sure it will be visible again.
                    if(isDropped == false) {
                        layoutParams = (RelativeLayout.LayoutParams) currentView.getLayoutParams();
                        layoutParams.topMargin = startPoint.y;
                        currentView.setLayoutParams(layoutParams);
                    }
                    currentView.setVisibility(View.VISIBLE);
                    currentView.invalidate();
                    v.invalidate();
                    // returns true; the value is ignored.
                    return true;

                // An unknown action type was received.
                default:
                    Logger.Error("Unknown action type received by OnDragListener.");
                    break;
            }
            return false;
        }
    }
}
