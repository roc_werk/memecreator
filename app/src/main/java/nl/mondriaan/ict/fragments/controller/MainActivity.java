package nl.mondriaan.ict.fragments.controller;

import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import nl.mondriaan.ict.fragments.R;
import nl.mondriaan.ict.fragments.model.MemeImage;
import nl.mondriaan.ict.fragments.model.MemeModel;
import nl.mondriaan.ict.fragments.util.ImageHelper;

public class MainActivity extends AppCompatActivity {

    public static final String MEME_APP = "MEME_CREATOR";
    public static final String LOG_VARIABLE = "MEME_CREATOR";

    private BottomPictureFragment bottomPictureFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Setting up the toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.memecreator_toolbar);
        setSupportActionBar(myToolbar);

        // Creating the model
        MemeModel model = new MemeModel();
        List<MemeImage> imagesResources = new ArrayList<>();
        Resources res = getResources();
        imagesResources.add(new MemeImage(res, R.drawable.actual_advice_mallard, "Advice Mallard"));
        imagesResources.add(new MemeImage(res, R.drawable.spiderman_peter_parker, "Peter Parker"));
        imagesResources.add(new MemeImage(res, R.drawable.troll_face, "Troll face"));
        imagesResources.add(new MemeImage(res, R.drawable.yao_ming, "Yao Ming"));
        model.setImages(imagesResources);

        // Setting the model so the fragment can update the text in the model
        TopSectionFragment topFragment = (TopSectionFragment) getSupportFragmentManager().findFragmentById(R.id.fragment);
        topFragment.setModel(model);

        // Registering the model so the fragment can listen to updates
        bottomPictureFragment = (BottomPictureFragment) getSupportFragmentManager().findFragmentById(R.id.fragment2);
        model.register(bottomPictureFragment);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share:
                share();
                return true;

            default:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // You need to inflate the menu before it will be shown
        getMenuInflater().inflate(R.menu.memecreator_actionbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    public Intent getShareIntent() {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("image/*");
        shareIntent.putExtra(Intent.EXTRA_STREAM, ImageHelper.getBitmapURI(this, bottomPictureFragment.getView(), "Meme", "Awesome meme created by the Meme Creator"));
        return shareIntent;
    }

    public void share() {
        Intent sendIntent = getShareIntent();
        String title = getResources().getString(R.string.share_title);
        // Create the application chooser
        Intent chooser = Intent.createChooser(sendIntent, title);
        // Look for applications that can handle the contents of the share intent (aka the image)
        if(sendIntent.resolveActivity(getPackageManager()) != null) {
            // Start the application chooser
            startActivity(chooser);
        }
    }
}
