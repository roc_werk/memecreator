package nl.mondriaan.ict.fragments.controller;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.FileProvider;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import nl.mondriaan.ict.fragments.R;
import nl.mondriaan.ict.fragments.model.MemeImage;
import nl.mondriaan.ict.fragments.model.MemeModel;
import nl.mondriaan.ict.fragments.util.Logger;

public class TopSectionFragment extends Fragment {

    private EditText topTextInput;
    private EditText bottomTextInput;
    private EditText topTextSizeInput;
    private EditText bottomTextSizeInput;
    private Button photoButton;
    private Button updateButton;
    private String currentPhotoPath;
    private Spinner memeDropdown;
    private MemeModel model;

    private static final int REQUEST_IMAGE_CAPTURE = 2;

    public void setModel(MemeModel model) {
        this.model = model;
        // Update the views based on the new model
        updateViews();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // If the result is not okay we want to do nothing
        if(resultCode != Activity.RESULT_OK) {
            return;
        }

        // If the request code is REQUEST_IMAGE_CAPTURE we know the camera succesfully took a photo
        if(requestCode == REQUEST_IMAGE_CAPTURE) {
            updateImage();
        }
    }

    private void updateImage() {
        Bitmap bitmap = BitmapFactory.decodeFile(currentPhotoPath);
        this.model.setImage(new MemeImage("Photo", bitmap));
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.top_section_fragment, container, false);

        memeDropdown = (Spinner) view.findViewById(R.id.memeDropdown);
        memeDropdown.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                MemeImage selectedMemeImage = (MemeImage) parent.getSelectedItem();
                if(model != null) {
                   model.setImage(selectedMemeImage);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        photoButton = (Button) view.findViewById(R.id.buttonPhoto);
        photoButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v) {
                        dispatchTakePictureIntent();
                    }
                }
        );

        updateButton = (Button) view.findViewById(R.id.button);
        updateButton.setOnClickListener(
                new View.OnClickListener(){
                    public void onClick(View v) {
                        updateText();
                    }
                }
        );

        // Created a new interface which we can use for both the topTextInput and bottomTextInput
        TextWatcher textWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {}

            @Override
            public void afterTextChanged(Editable s) {
                updateText();
            }
        };

        topTextInput = (EditText) view.findViewById(R.id.topTextInput);
        bottomTextInput = (EditText) view.findViewById(R.id.bottomTextInput);
        topTextSizeInput = (EditText) view.findViewById(R.id.topTextSizeInput);
        bottomTextSizeInput = (EditText) view.findViewById(R.id.bottomTextSizeInput);

        topTextInput.addTextChangedListener(textWatcher);
        bottomTextInput.addTextChangedListener(textWatcher);

        return view;
    }

    public void updateText() {
        try {
            model.setText(topTextInput.getText().toString(), bottomTextInput.getText().toString());
            model.setTextSize(Integer.parseInt(topTextSizeInput.getText().toString()), Integer.parseInt(bottomTextSizeInput.getText().toString()));
        }
        catch (Exception e){
            Logger.Error(e.toString());
        }
    }

    private void updateViews() {
        initDropdown();
        initTextFields();
    }

    private void initDropdown() {
        if(model != null) {
            // Create an ArrayAdapter using the string array and a default spinner layout
            ArrayAdapter<MemeImage> adapter = new ArrayAdapter<>(getContext(), android.R.layout.simple_spinner_dropdown_item, this.model.getImages());
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            // Apply the adapter to the spinner
            memeDropdown.setAdapter(adapter);
        }
    }

    private void initTextFields() {
        if(model != null) {
            topTextInput.setText(model.getTopText());
            bottomTextInput.setText(model.getBottomText());
            topTextSizeInput.setText(String.valueOf(model.getTopTextSize()));
            bottomTextSizeInput.setText(String.valueOf(model.getBottomTextSize()));
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = getContext().getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        currentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getActivity().getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                Logger.Error(ex.toString());
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                Uri photoURI = FileProvider.getUriForFile(this.getContext(),
                        "nl.mondriaan.ict.fileprovider",
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
            }
        }
    }
}
