package nl.mondriaan.ict.fragments.model;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class MemeImage {

    private int resourceID = -1;
    private String name;
    private Bitmap image;

    public MemeImage(String name, Bitmap image) {
        this.image = image;
        this.name = name;
    }

    public MemeImage(Resources res, int resourceID, String name) {
        setImage(res, resourceID);
        this.name = name;
    }

    public Bitmap getImage() {
        return image;
    }

    public void setImage(Bitmap image) {
        this.image = image;
    }

    public void setImage(Resources res, int resourceID) {
        this.resourceID = resourceID;
        setImage(BitmapFactory.decodeResource(res, resourceID));
    }

    /**
     * Returns the resource ID or -1 if no resource id was set;
     * @return resourceID or -1
     */
    public int getResourceID() {
        return resourceID;
    }

    public void setResourceID(int resourceID) {
        this.resourceID = resourceID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof MemeImage) {
            MemeImage mi = (MemeImage) obj;
            if(mi.getName().equals(name) && mi.getResourceID() == resourceID) return true;
        }
        return false;
    }
}
