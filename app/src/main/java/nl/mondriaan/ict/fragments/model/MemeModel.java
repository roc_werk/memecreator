package nl.mondriaan.ict.fragments.model;

import java.util.ArrayList;
import java.util.List;

public class MemeModel {

    // Creating a interface so we can be sure the registered class has the function update
    public interface MemeListener {
        void update(String top, String bottom);
        void updateImage(MemeImage memeImage);
        void updateTextSize(int top, int bottom);
    }

    private String topText;
    private String bottomText;
    private int topTextSize;
    private int bottomTextSize;
    // memeImages contains the resource IDs of the drawable
    private List<MemeImage> memeImages = new ArrayList<>();
    private MemeImage currentImage;
    private List<MemeListener> listeners = new ArrayList<>();

    public MemeModel() {
        topText = "";
        bottomText = "";
        topTextSize = 30;
        bottomTextSize = 30;
    }

    /**
     * The register method to add all the listeners
     * @param listener the listening class which will have the update  implemented
     */
    public void register(MemeListener listener) {
        listeners.add(listener);
    }

    /**
     * This method will call the update function of the listeners classes
     */
    private void updateText() {
        for(MemeListener listener : listeners) {
            listener.update(topText, bottomText);
        }
    }

    /**
     * This method will call the update function of the listeners classes
     */
    private void updateImage() {
        for(MemeListener listener : listeners) {
            listener.updateImage(currentImage);
        }
    }

    /**
     * This method will call the update function of the listeners classes
     */
    private void updateTextSize() {
        for(MemeListener listener : listeners) {
            listener.updateTextSize(topTextSize, bottomTextSize);
        }
    }

    /*
     * Getters and Setters
     *
     * NOTE: When a setter is called the dispatch method will be executed so all the listeners
     * will know a text is updated.
     */

    /**
     * Set/Override the complete list of memeImages.
     * @param imagesList List of MemeImage
     */
    public void setImages(List<MemeImage> imagesList) {
        memeImages = imagesList;
        currentImage = memeImages.get(0);
        updateImage();
    }

    public List<MemeImage> getImages() {
        return memeImages;
    }

    public void setImage(MemeImage memeImage) {
        currentImage = memeImage;
        updateImage();
    }

    public void setText(String topText, String bottomText) {
        this.topText = topText;
        this.bottomText = bottomText;
        // Notify all the listeners our data has been changed
        updateText();
    }

    public String getTopText() {
        return topText;
    }

    public void setTopText(String topText) {
        this.topText = topText;
        // Notify all the listeners our data has been changed
        updateText();
    }

    public String getBottomText() {
        return bottomText;
    }

    public void setBottomText(String bottomText) {
        this.bottomText = bottomText;
        // Notify all the listeners our data has been changed
        updateText();
    }

    public void setTextSize(int topText, int bottomText) {
        this.topTextSize = topText;
        this.bottomTextSize = bottomText;
        // Notify all the listeners our data has been changed
        updateTextSize();
    }

    public int getTopTextSize() {
        return topTextSize;
    }

    public void setTopTextSize(int topTextSize) {
        this.topTextSize = topTextSize;
        updateTextSize();
    }

    public int getBottomTextSize() {
        return bottomTextSize;
    }

    public void setBottomTextSize(int bottomTextSize) {
        this.bottomTextSize = bottomTextSize;
        updateTextSize();
    }
}
