package nl.mondriaan.ict.fragments.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.net.Uri;
import android.provider.MediaStore;
import android.view.View;

public class ImageHelper {

    public static Bitmap viewToBitmap(View view) {
        Bitmap bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);
        return bitmap;
    }

    public static Uri getBitmapURI(ContentResolver cr, Bitmap bitmap, String title, String description) {
        String bitmapPath = MediaStore.Images.Media.insertImage(cr, bitmap, title, description);
        return Uri.parse(bitmapPath);
    }

    public static Uri getBitmapURI(ContentResolver cr, View view, String title, String description) {
        Bitmap bitmap = ImageHelper.viewToBitmap(view);
        return ImageHelper.getBitmapURI(cr, bitmap, title, description);
    }

    public static Uri getBitmapURI(Activity activity, View view, String title, String description) {
        Bitmap bitmap = ImageHelper.viewToBitmap(view);
        return ImageHelper.getBitmapURI(activity.getContentResolver(), bitmap, title, description);
    }

    public static Uri getBitmapURI(Activity activity, Bitmap bitmap, String title, String description) {
        return ImageHelper.getBitmapURI(activity.getContentResolver(), bitmap, title, description);
    }

    public static Bitmap setBitmapSize(View imageView, String photoPath) {
        // Get the dimensions of the View
        int targetW = imageView.getWidth();
        int targetH = imageView.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(photoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return BitmapFactory.decodeFile(photoPath, bmOptions);
    }
}
