package nl.mondriaan.ict.fragments.util;


import android.util.Log;

import nl.mondriaan.ict.fragments.controller.MainActivity;

public abstract class Logger {

    public static void Debug(String value) {
        Log.d(MainActivity.LOG_VARIABLE, value);
    }

    public static void Debug(int value) {
        Logger.Debug("" + value);
    }

    public static void Debug(double value) {
        Logger.Debug("" + value);
    }

    public static void Debug(float value) {
        Logger.Debug("" + value);
    }

    public static void Verbose(String value) {
        Log.v(MainActivity.LOG_VARIABLE, value);
    }

    public static void Verbose(int value) {
        Logger.Verbose("" + value);
    }

    public static void Verbose(double value) {
        Logger.Verbose("" + value);
    }

    public static void Verbose(float value) {
        Logger.Verbose("" + value);
    }

    public static void Error(String value) {
        Log.e(MainActivity.LOG_VARIABLE, value);
    }

    public static void Error(int value) {
        Logger.Error("" + value);
    }

    public static void Error(double value) {
        Logger.Error("" + value);
    }

    public static void Error(float value) {
        Logger.Error("" + value);
    }
}
